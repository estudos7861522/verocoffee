# Use uma imagem base do Nginx
FROM nginx:latest

# Copie o conteúdo do site para o diretório padrão do Nginx
COPY ../ /usr/share/nginx/html

# Expõe a porta 80
EXPOSE 80

# Comando para iniciar o servidor Nginx
CMD ["nginx", "-g", "daemon off;"]